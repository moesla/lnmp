# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH


HISTSIZE=500000
HISTTIMEFORMAT='|%Y/%m/%d %H:%M:%S| '
HISTCONTROL=ignoredups

export PROMPT_COMMAND='history -a; history -r'

export PS1="[\u@local.docker \W]\\$ "

export LESSCHARSET=utf-8
export LANG=ja_JP.UTF-8

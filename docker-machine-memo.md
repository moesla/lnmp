
** 初期設定のディスク容量だと少なすぎるので大容量の docker を作成する

* 400G の docker-machine を作成する
```
docker-machine create -d virtualbox --virtualbox-disk-size 400000 project
```

* dokcer の環境を確認する
```
docker info | fgrep -i name
docker active
```

* docker を切り替える
```
eval "$(docker-machine env project)"
```

* コンテナの初期状態で作成されるディスク容量を大きくする
```
docker -d --storage-opt dm.basesize=20G
```
windows 環境だとむり？なようなので docker の設定ファイルを直接編集
```
/usr/local/etc/init.d/docker
```
の ``` /usr/local/bin/dockerd ``` の箇所に直接指定 ``` --storage-opt dm.basesize=200G ```
```
/usr/local/bin/dockerd -D -g "$DOCKER_DIR" -H unix:// $DOCKER_HOST $EXTRA_ARGS --storage-opt dm.basesize=200G  >> "$DOCKER_LOGFILE" 2>&1 &
```



* おまけ
    - docker 削除
    ``` docker-machine rm default ```

    - docker 一覧
    ``` docker-machine ls ```


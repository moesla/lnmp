開発に必要そうなもの環境セット
```
centos7
php7
mysql
redis
memcache
```

-----------------------------
# 設定

* STEP0 Docker Toolbox のインストール  
   [Docker Toolbox](https://github.com/docker/toolbox/releases)

* STEP1 ssh 鍵
```
mkdir ./.ssh
ssh-keygen -t rsa -f ./.ssh/id_rsa
cat ./.ssh/id_rsa.pub >> ./.ssh/authorized_keys
chmod 600 ./.ssh/authorized_keys
```

* STEP2 OS インストール ビルド
```
docker build -t centos:installed --file=./Dockerfile.000.install.centos ./
```

* STEP3 LNMP インストール ビルド
```
docker build -t lnmp:installed --file=./Dockerfile.001.install.lnmp ./

```

* STEP4 LNMP セットアップ ビルド
```
docker build -t lnmp:configured --file=./Dockerfile.002.lnmp.setting ./
```

* STEP5 mysql 初期化
```
sed -ri "s|^password = MYSQL_PASSWRD|password = `fgrep 'temporary password' /var/log/mysqld.log | awk '{print $11}'`|g" /root/.mysql_local.3306

SET GLOBAL validate_password_length=4;
SET GLOBAL validate_password_policy=LOW;
SET PASSWORD FOR root@"localhost"=PASSWORD('hoge');
grant ALl ON *.* to root@'%' identified by 'hoge';
grant ALL ON *.* to root@'127.0.0.1' identified by 'hoge';
delete from mysql.user where user='';
FLUSH PRIVILEGES;
```

-------------------------------------------
# 設定ファイル
* profile  
` /etc/profile.d/docker_profile.sh `  
` /etc/profile.d/php70.sh `  

* php / php-fpm  
` /etc/opt/remi/php70/ `

* mysqld  
` /etc/my.cnf `

* nginx  
`  /etc/nginx/ `

* redis  
` /etc/redis.conf `

----------------------------------------------
# ログ

* nginx  
` /var/log/nginx.access.log `  
` /var/log/nginx.error.log `  

* php-fpm  
` /var/log/php-fpm.error.log `

* php  
` /var/log/php.error.log `

* redis  
` /var/log/redis.log `

* mysqld  
` /var/log/mysqld.log `  
` /var/lib/mysql/query.log `  
` /var/lib/mysql/slow-query.log ` 


---------------------------
# docker

* コンテナ起動
```
docker run -d \
    --name gfc_v1 \
    --privileged \
    -p 1022:22 \
    -p 1080:80 \
    -v /media/foo:/root/bar:rw \
    centos:installed
```

* docker へ接続
```
docker exec -it gfc_v1 bash
```

* docker image を commit
```
docker stop gfc_v1
docker ps
docker ps -a
docker commit gfc_v1 gfc:v1
docker images

```


[docker-machine メモ](/docker-machine-memo.md)


----------------------------------
# centos

* サービス登録
```
systemctl enable mysqld.service
systemctl enable redis.service
systemctl enable memcached.service
systemctl enable nginx.service
systemctl enable php70-php-fpm.service
```

* サービス削除
```
systemctl disable mysqld.service
systemctl disable redis.service
systemctl disable memcached.service
systemctl disable nginx.service
systemctl disable php70-php-fpm.service
```

* サービス起動
```
systemctl start sshd.service
systemctl start mysqld.service
systemctl start redis.service
systemctl start memcached.service
systemctl start nginx.service
systemctl start php70-php-fpm.service
```

* サービス停止
```
systemctl stop mysqld.service
systemctl stop redis.service
systemctl stop memcached.service
systemctl stop nginx.service
systemctl stop php70-php-fpm.service
```

* サービス再起動
```
systemctl restart mysqld.service
systemctl restart redis.service
systemctl restart memcached.service
systemctl restart nginx.service
systemctl restart php70-php-fpm.service
```